import { Box } from "@mui/material";
import { styled } from "@mui/material/styles";

const FooterBox = styled(Box)(() => ({
  height: '50px',
  position: 'sticky',
  borderTop: '2px solid #ececec',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  bottom: 0,
  width: '100%',
  zIndex: -1,
}))


const Footer = () => {
  return (
    <FooterBox>
      2023 Palms Bet
    </FooterBox>
  )
}

export default Footer;