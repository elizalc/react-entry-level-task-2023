import { Box } from "@mui/material";
import { styled } from "@mui/material/styles";

const HeaderBox = styled(Box)(() => ({
  height: '70px',
  position: 'fixed',
  backgroundColor: '#fff',
  top: 0,
  width: '100%',
}))


const Header = () => {
  return (
    <HeaderBox />
  )
}

export default Header;