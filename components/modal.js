import { useEffect, useLayoutEffect, useState } from 'react';
import Image from 'next/image';
import { Box, Fade, styled, keyframes } from '@mui/material';

const spin = keyframes`
  100% {
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
  }
`;

const ModalBox = styled(Box)(({ top, left, width }) => ({
  top,
  left,
  width,
  height: '200px',
  position: 'fixed',
  animation: `${spin} .7s linear`,
  animationFillMode: 'forwards',
  zIndex: 20,
  background: 'white',
  '> button': {
    position: 'absolute',
    top: 20,
    right: 20,
    background: 'red',
    width: '20px',
    height: '20px',
    zIndex: 30
  }
}))

const ImageBox = styled(Box)(() => ({
  position: 'relative',
  height: '100%',
}))

const Link = styled('a')(() => ({
  fontSize: '18px',
  fontWeight: 'bold',
  position: 'absolute',
  zIndex: 25
}))

const Modal = ({ game }) => {
  const [isOpen, setIsOpen] = useState(false)
  const [showLink, setShowLink] = useState(false)

  useEffect(() => {
    if (game?.rect) {
      setIsOpen(true)
      setTimeout(() => {
        setShowLink(true)
      }, 700)
    }
  }, [game?.rect])

  return (
    <Fade in={isOpen} timeout={200}>
      <ModalBox top={game?.rect.top} left={game?.rect.left} width={game?.rect.width}>
        <button onClick={() => setIsOpen(false)}>X</button>
        {showLink && (
          <Link target='_blank' href={game.link}>{game?.link}</Link>
        )}
        <ImageBox>
          <Image src={game?.icon} fill alt={game?.name} style={{ objectFit: 'contain' }} />
        </ImageBox>
      </ModalBox>
    </Fade>
  )
}
  


export default Modal