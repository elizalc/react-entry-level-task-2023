module.exports = {
  reactStrictMode: true,
  swcMinify: true,
  images: {
  unoptimized: true,
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'cdn3.ctrgs.com',
        port: '',
      },
      {
        protocol: 'https',
        hostname: 'cdn1.ctrgs.com',
        port: '',
      },
      {
        protocol: 'https',
        hostname: 'www.palmsbet.com',
        port: '',
      },
      {
        protocol: 'https',
        hostname: 'bl-ecasino.palmsbet.com',
        port: '',
      },
      {
        protocol: 'https',
        hostname: 's3.eu-central-1.amazonaws.com',
        port: '',
      }
    ],
  },
}