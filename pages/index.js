import { useState } from "react";
import Image from 'next/image';
import { Box } from "@mui/material";
import Header from "../components/header";
import { styled } from "@mui/material/styles";
import Footer from "../components/footer";
import Modal from "../components/modal";

const MainSection = styled(Box)(({ theme }) => ({
  marginTop: '70px',
  backgroundColor: '#fff',
  display: 'grid',
  gap: '1rem',
  gridTemplateColumns: 'repeat(2, 1fr)',
  [theme.breakpoints.up('md')]: {
    gridTemplateColumns: 'repeat(3, 1fr)',
  },
  [theme.breakpoints.up('lg')]: {
    gridTemplateColumns: 'repeat(6, 1fr)',
  }
}))

const CardBox = styled(Box)(() => ({
  position: 'relative',
  height: '200px',
  cursor: 'pointer',
  transition: 'all .150s ease-in-out',
  '&:hover': {
    transform: 'scale(1.1)'
  }
}))

export default function ({ gamesList }) {
  const [selectedGame, setSelectedGame] = useState(null)

  return (
    <main>
      <Header />
      <MainSection>
        {gamesList.map((game) => (
          <CardBox
            key={game.name}
            onClick={(e) =>  {
              const rect = e.target.getBoundingClientRect();
              setSelectedGame({
                ...game,
                rect,
              })
            }}
          >
            <Image fill src={game.icon} alt={game.name} style={{ objectFit: 'contain' }} />
          </CardBox>
        ))}
      </MainSection>
      <Modal game={selectedGame} key={selectedGame?.name} />
      <Footer />
    </main>
  )
}

export const getStaticProps = async () => {
  const res = await fetch('https://www.ct-interactive.dev/samples/')

  const data = await res.json()

  return { props: { gamesList: data.games_list.map(item => ({
    ...item,
    icon: item.icon.includes('http') ?  item.icon : `https:${item.icon}`
  })) } }
}